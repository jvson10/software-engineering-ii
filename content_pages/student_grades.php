<?php
session_start();
$conn = mysqli_connect("localhost","root","","grading_system");
$id = $_SESSION['u_id'];

echo "<div class='container-fluid'>";
		echo "<table class='table table-striped table-dark'>";
			echo "<thead>";
				echo "<tr>";
					echo "<th width='200px'>";
						echo "Subjects:";
					echo "</th>";
					echo "<th width='150px'>";
						echo "Midterm Grade";
					echo "</th>";
					echo "<th width='150px'>";
						echo "Final Grade";
					echo "</th>";
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
				$sql = "SELECT * FROM grades_table gt JOIN class_table ca ON gt.class_id = ca.class_id WHERE student_id='$id' ";
				$result = mysqli_query($conn,$sql);
				$count_midterm=0;
				$total_midterm = 0;
				$count_finals = 0;
				$total_finals = 0;

				$NG_count_M = 0;
				$NG_count_F = 0;
				while($row = mysqli_fetch_array($result)){
					
					
					echo "<tr>";
						echo "<td>".$row['subject_name']."</td>";
						if(!empty($row['grade_midterm'])){
							if(preg_match('~[0-9]~',$row['grade_midterm'])){
								$num = (float) $row['grade_midterm'];
								echo "<td>".number_format($num,1)."</td>";
								$total_midterm += (float) $row['grade_midterm'];
								$count_midterm++;
							}else{
								echo "<td>".$row['grade_midterm']."</td>";
							}
						}else{
							echo "<td>NG</td>";
							$NG_count_M ++;
						}
						if(!empty($row['grade_final'])){
							if(preg_match('~[0-9]~',$row['grade_final'])){
								$num = (float) $row['grade_final'];
								echo "<td>".number_format($num,1)."</td>";
								$total_finals += (float) $row['grade_final'];
								$count_finals ++;
							}else{
								echo "<td>".$row['grade_final']."</td>";
							}
						}else{
							echo "<td>NG</td>";
							$NG_count_F++;
						}
					echo "</tr>";
				}
				
				
				echo "<tr>";
						echo "<td></td>";
						if($NG_count_M<1){
							$ave_midterm = $total_midterm / $count_midterm;
							if($total_midterm == 0){
								echo "<td>Ave: 5.0</td>";
							}else{
								echo "<td>Ave: ".number_format($ave_midterm,1)."</td>";
							}
						}else{
							echo "<td>Ave:N/A/td>";
						}
						if($NG_count_F<1){
							$ave_finals = $total_finals / $count_finals;
							if($total_finals == 0){
								echo "<td>Ave: 5.0</td>";
							}else{
								echo "<td>Ave: ".number_format($ave_midterm,1)."</td>";
							}
						}else{
							echo "<td>Ave:N/A</td>";
						}
					echo "</tr>";
			echo "</tbody>";
		echo "</table>";
	echo "</div>";
?>