<?php
session_start();
$conn = mysqli_connect("localhost","root","","grading_system");
$id = $_SESSION['u_id'];

echo "<div class='container-fluid' style='width:75%; backgroundcolor:#2F3030;'>";
		echo "<table class='table table-striped table-dark'>";
			echo "<thead>";
				echo "<tr>";
					echo "<th width='100%'>";
                        echo "Schedule Class Record :";
					echo "</th>";
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
				$sql = "SELECT * FROM `users_table` WHERE `user_type` = 'Student' ORDER BY `sched` ASC";
				$result = mysqli_query($conn,$sql);
				while($row = mysqli_fetch_array($result)){
					echo "<tr>";
                        //echo "<td><a class='subject_popup' data-fancybox data-type='ajax' data-src='content_pages/view_class_record.php?subject_id=".$row['sem_id']."' href='javascript:;'>".$row['sem_id']."</a></td>";
                        echo "<td>Schedule:".$row['sched']." ".$row['user_uid']." ".$row['user_fname']." ".$row['user_lname']."</td>";
					echo "</tr>";
				}
			echo "</tbody>";
		echo "</table>";
	echo "</div>";
?>