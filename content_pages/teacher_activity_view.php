<?php
session_start();
$conn = mysqli_connect("localhost","root","","grading_system");
$id = $_SESSION['u_id'];
$subject_id = $_GET['subject_id'];
?>

<div class='container-fluid' id='subjpage'>
	<?php
	$result = mysqli_query($conn,"SELECT * FROM class_table WHERE class_id = '$subject_id'");
	$subject = mysqli_fetch_object($result);
	echo "<h1 style='color:white;'>".$subject->subject_name."</h1>";
	?>
	<div class='row'>
			<?php
			echo "<table class='table table-striped table-dark'>";
			echo "<thead>";
				echo "<tr>";
					echo "<th width='100px'>ID</th>";
					echo "<th width='100px'>";
						echo "Activity Name: ";
					echo "</th>";
					echo "<th width='100px'>";
						echo "Midterm Grade: ";
					echo "</th>";
					echo "<th width='100px'>";
						echo "Final Grade: ";
					echo "</th>";
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
				$sql = "SELECT * FROM activity_table t JOIN users_table ut ON t.teacher_id = ut.user_id WHERE class_id = '$subject_id'";
				$result = mysqli_query($conn,$sql);
				while($row = mysqli_fetch_array($result)){

					if((float)$row['grade_midterm']>3){
						$grade = 5;
					}else{
						$grade = $row['grade_midterm'];
					}

					if((float)$row['grade_final']>3){
						$grade_fin = 5;
					}else{
						$grade_fin = $row['grade_final'];
					}


					echo "<tr>";
						echo "<td><span>".$row['activity_id']."</span></td>";
						echo "<td ><span class='student'>".$row['activity_name'];
						if(preg_match('~[0-9]~',$row['grade_midterm'])){

							echo "<td class='midterm_td'><span  id ='".$row['user_id']."m' class='val' value = '".$row['grade_midterm']."'>".
							number_format($grade,1)."</span>
								<input class='id' type='hidden' value='".$row['user_id']."'>
								<input class='type' type='hidden' value='midterm'>
								<input class='sub' type='hidden' value='".$row['class_id']."'>
							</td>";
						}else if(empty($row['grade_midterm'])){

							echo "<td class='midterm_td'><span  id ='".$row['user_id']."m' class='val' value = '".$row['grade_midterm']."'>NG</span>
								<input class='id' type='hidden' value='".$row['user_id']."'>
								<input class='type' type='hidden' value='midterm'>
								<input class='sub' type='hidden' value='".$row['class_id']."'>
							</td>";

						}else if(preg_match('~[A-Z]~',$row['grade_midterm'])){

							echo "<td class='midterm_td'><span id ='".$row['user_id']."m' class='val' value = '".$row['grade_midterm']."'>".$row['grade_midterm']."</span>
								<input class='id' type='hidden' value='".$row['user_id']."'>
								<input class='type' type='hidden' value='midterm'>
								<input class='sub' type='hidden' value='".$row['class_id']."'>
							</td>";
						}

						if(preg_match('~[0-9]~',$row['grade_final'])){
							echo "<td class='midterm_td'><span  id ='".$row['user_id']."f' class='val' value = '".$row['grade_final']."'>".number_format($grade_fin,1)."</span>
								<input class='id' type='hidden' value='".$row['user_id']."'>
								<input class='type' type='hidden' value='final'>
								<input class='sub' type='hidden' value='".$row['class_id']."'>
							</td>";
						}else if(empty($row['grade_final'])){
							echo "<td class='midterm_td'><span  id ='".$row['user_id']."f' class='val' value = '".$row['grade_final']."'>NG</span>
								<input class='id' type='hidden' value='".$row['user_id']."'>
								<input class='type' type='hidden' value='final'>
								<input class='sub' type='hidden' value='".$row['class_id']."'>
							</td>";
						}else if(preg_match('~[A-Z]~',$row['grade_final'])){
							echo "<td class='midterm_td'><span id ='".$row['user_id']."f' class='val' value = '".$row['grade_final']."'>".$row['grade_final']."</span>
								<input class='id' type='hidden' value='".$row['user_id']."'>
								<input class='type' type='hidden' value='final'>
								<input class='sub' type='hidden' value='".$row['class_id']."'>
							</td>";
						}
					echo "</tr>";
				}
			echo "</tbody>";
		echo "</table>";
			?>
	</div>
</div>