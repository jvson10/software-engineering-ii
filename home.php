<!DOCTYPE html>
<?php 
	session_start();
	$conn = mysqli_connect("localhost","root","","")
?>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>GradeHub</title>
	<?php 
		echo "<link rel='stylesheet' href = 'scripts/bootstrap/css/bootstrap.min.css'>";
		echo "<link rel='stylesheet' href = 'scripts/jquery/jquery-confirm.min.css'>";
		echo "<style>";
			include 'scripts/style.css';
			include 'scripts/jquery/jquery.fancybox.min.css';
		echo "</style>";
	?>
</head>
<body>
	<div class='container-fluid'>
		<div class='row' style='background-color:black;height:150px'>
		</div>
		<div class='row'>
			<div class='col-lg-4'>
				<div class='box' style='background-color:black;'>
					<div class='col-lg-12' id='user_box' style='background-color:black;text-align:center;'>
						<div class='row'>
							<div class='col-lg-12'>
								<div id='form'>
									<?php
									if(isset($_SESSION['u_uid'])){
										echo "Welcome!";
										echo "<form style='margin-top:20%' action='backgroundscripts/logout.php' method ='POST'>";
											echo "<span>";
											echo "<button type='submit' class='btn btn-warning'  name='submit'>Logout</button>";
											echo "</span>";
										echo "</form>";
									}else{
										echo "<form class='signup-form' action='backgroundscripts/login.inc.php' method='POST' id='login_form'>
										<img src='pihotos/logo.png'>
										<input class='form-control' type='text' name='uid' placeholder = 'Username' style='margin-top:10%;'><br><br>
										<input class='form-control' type='password' name='pass' placeholder = 'Password'><br><br>
										<button class = 'btn btn-warning' type='submit' name='submit'>Log In</button>
										</form>";
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
				<div class='col-lg-8' style='background-color:black;color:white;'>
					<?php
					if(isset($_SESSION['u_uid'])){
						if($_SESSION['u_type']==='Student'){
							echo "<div style='background-color:black;' id='content'>
							</div>";
						}else{
							echo "<div style='background-color:black;' id='teacher_content'>
							</div>"; 
						}
					}
					?>
				</div>
				
		</div>
			
	</div>

</body>
<?php
	echo "<script src= 'scripts/jquery/jquery-3.3.1.min.js'></script>";
	echo "<script src = 'scripts/jquery/jquery-confirm.min.js'></script>";
	echo "<script src='scripts/jquery/jquery.fancybox.min.js'></script>";
	echo "<script src= 'scripts/bootstrap/js/bootstrap.min.js'></script>"; 
?>
<script type="text/javascript">
	$('#content').load('content_pages/student_view_sems.php'); //student_grades
	//$('#teacher_content').load('content_pages/view_sems.php'); //teacher_view_grades
	$('#teacher_content').load('content_pages/class_record.php');
	$(document).ready(function(){
		$('.subject_popup').fancybox({
			autoSize:false,
			touch:false,
			height:700,
			width:640
		});
	});

	$(document).on('click','.midterm_td',function(){
		grade = $(this).find('.val').attr('value');
		id = $(this).find('.id').attr('value');
		type = $(this).find('.type').attr('value');
		subject = $(this).find('.sub').attr('value');
		//$(this).find('.val').replaceWith("<input class='input_grade' style='width:50%' type='text' value = '"+grade+"'>");

		//$('.input_grade').blur(function(){
			//input_grade = $('.input_grade').val();
			//$(this).replaceWith("<span>"+input_grade+"</span>");
		//});
		$.confirm({
			title:'Update/Edit',
			content:'' + 
			'<form action="" class="formName">' +
		    '<div class="form-group">' +
		    '<label>Update grade:</label>' +
		    '<input type="text" placeholder="Updated Grade" class="name form-control" required />' +
		    '</div>' +
		    '</form>',
		    type: 'orange',
		    buttons: {
		    	formSubmit: {
		    		text: 'Submit',
		    		btnClass: 'btn-warning',
		    		action: function() {
		    			var grade = this.$content.find('.name').val();
		    			if(!grade){
		    				$.alert('Please provide a grade');
		    				return false;
		    			}else if((grade>=1 && grade <=5) || grade=='INC' || grade == 'NC' ){
		    				$.confirm({
								title:'',
		    					content:'Confirm Changes?',
		    					buttons:{
		    						confirm: function(){
		    							$.ajax({
		    								type: 'POST',
		    								url: 'backgroundscripts/update_grade.php',
		    								data:{
		    									newgrade: grade,
		    									stud_id: id,
		    									type: type,
		    									subject: subject
		    								},success:function(e){
		    									if(type=='midterm'){
		    										if(grade == 'NC' || grade == 'INC'){
			    										$('#'+id+'m').replaceWith("<span>"+grade+"</span>");
			    									}else{
			    										if(grade>3){
			    											grade = 5;
			    										}
			    										$('#'+id+'m').replaceWith("<span>"+parseFloat(grade).toFixed(1)+"</span>");
			    									}
		    									}else if(type=='final'){
		    										if(grade == 'NC' || grade == 'INC'){
			    										$('#'+id+'f').replaceWith("<span>"+grade+"</span>");
			    									}else{
			    										if(grade>3){
			    											grade = 5;
			    										}
			    										$('#'+id+'f').replaceWith("<span>"+parseFloat(grade).toFixed(1)+"</span>");
			    									}
		    									}
		    								}
		    							});
		    						},
		    						cancel: function(){
		    							return false;
		    						}
		    					}
		    				});
		    			}else{
		    				$.alert('Please provide a valid grade');
		    				return false;
		    			}
		    		}
		    	}, 
		    	cancel :function(){

		    	},
		    	
		    },
		    onContentReady: function(){
	    		var jc = this;
	    		this.$content.find('form').on('submit',function(e){
	    			e.preventDefault();
	    			jc.$$formSubmit.trigger('click');
	    		});
	    	}
		});
	});

</script>
</html>