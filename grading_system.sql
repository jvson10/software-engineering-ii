-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2019 at 02:14 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grading_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `class_table`
--

CREATE TABLE `class_table` (
  `class_id` int(11) NOT NULL,
  `subject_name` varchar(50) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `sem_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_table`
--

INSERT INTO `class_table` (`class_id`, `subject_name`, `teacher_id`, `sem_id`) VALUES
(1, 'Cataclysm 1102', 2, 1),
(2, 'Burning Crusade 12N', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `grades_table`
--

CREATE TABLE `grades_table` (
  `grading_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `grade_midterm` varchar(5) NOT NULL,
  `grade_final` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades_table`
--

INSERT INTO `grades_table` (`grading_id`, `class_id`, `student_id`, `grade_midterm`, `grade_final`) VALUES
(1, 1, 1, '3', '1.0'),
(2, 1, 3, '1', '5.0'),
(3, 1, 4, '3', '1.0'),
(4, 1, 5, '1.7', '3.0'),
(5, 2, 4, '2.7', '1.2'),
(6, 2, 1, '1', '3.0'),
(7, 2, 3, '1.3', '3.0'),
(8, 2, 5, '1', '2.3'),
(9, 2, 6, '2', '2'),
(10, 3, 1, 'INC', '1'),
(11, 3, 6, '3.0', '3');

-- --------------------------------------------------------

--
-- Table structure for table `sems_table`
--

CREATE TABLE `sems_table` (
  `sem_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sems_table`
--

INSERT INTO `sems_table` (`sem_id`, `teacher_id`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_table`
--

CREATE TABLE `users_table` (
  `user_id` int(11) NOT NULL,
  `user_uid` varchar(50) NOT NULL,
  `user_fname` varchar(50) NOT NULL,
  `user_lname` varchar(50) NOT NULL,
  `user_pass` varchar(50) NOT NULL,
  `user_type` enum('Student','Teacher') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_table`
--

INSERT INTO `users_table` (`user_id`, `user_uid`, `user_fname`, `user_lname`, `user_pass`, `user_type`) VALUES
(1, '15105923', 'Orgrim', 'Doomhammer', '1234', 'Student'),
(2, '15102579', 'Gul\'dan', 'Stormreaver', 'p4ssw0rd', 'Teacher'),
(3, '15105922', 'Durotan', 'Frostwolf', '1234', 'Student'),
(4, '15105921', 'Warchief', 'Blackhand', '1234', 'Student'),
(5, '15105924', 'Grom', 'Hellscream', '1234', 'Student'),
(6, '15105925', 'Arthas', 'Menethil', '1234', 'Student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class_table`
--
ALTER TABLE `class_table`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `grades_table`
--
ALTER TABLE `grades_table`
  ADD PRIMARY KEY (`grading_id`);

--
-- Indexes for table `users_table`
--
ALTER TABLE `users_table`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class_table`
--
ALTER TABLE `class_table`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grades_table`
--
ALTER TABLE `grades_table`
  MODIFY `grading_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users_table`
--
ALTER TABLE `users_table`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
